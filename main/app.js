function addTokens(input, tokens){
    /*
        * Testare validitate parametrii
    */
    if (typeof input !== 'string') {
        throw new Error('Input should be a string');
    }    
    if (input.length < 6) {
        throw new Error('Input should have at least 6 characters');
    }
    let validArrayFormat = true;
    /*
        * Verificare fiecare element al vectorului
        * (Obs) Cred ca ar fi mai eficient sa folosesc un for
        * clasic fiindca as avea acces la 'break' si nu as mai testa
        * redundant o data ce flag-ul a fost setat pe false din orice motiv
    */
    tokens.forEach((e) => {
        /*
            * Verificare ca elementul sa fie obiect
            * In caz contrar, flagul este setat pe false
        */
        if (typeof e !== 'object') {
            validArrayFormat = false;
        } else {
            let keys = Object.keys(e);
            let values = Object.values(e);
            /*
                * Verificare ca fiecare cheie din obiect
                * sa fie denumita 'tokenName'
                * In caz contrar, flag-ul 'validArrayFormat' definit
                * mai sus va fi fals
            */
            keys.forEach((key) => {
                if (key !== 'tokenName') {
                    validArrayFormat = false;
                }
            });
            /*
                * Verificare ca fiecare valoare din obiect
                * sa fie tip string
                * In caz contrar, flag-ul 'validArrayFormat' definit
                * mai sus va fi fals
                * Exceptie fata de verificarea mai sus:
                *   - verific daca a fost deja schimbat flagul
                *   - daca a fost deja schimbat, nu are rost sa mai verific si valorile
            */
            if (validArrayFormat) {
                values.forEach((value) => {
                    if (typeof value !== 'string') {
                        validArrayFormat = false;
                    }
                });              
            }
        }
    });
    if (!validArrayFormat) {
        throw new Error('Invalid array format');
    }
    

    /*
        * Verificare daca inputul nu contine stringuri
        * de tipul '...'.
        * Daca nu contine, returnez inputul
    */
    if (input.indexOf('...') == -1) {
        return input;
    }

    /*
        * Construire si returnare string final
    */
    tokens.map((e) => {
        input = input.replace('...', '${' + e.tokenName + "}");
    });
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;